<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\TransactionController;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// DATABASE MANAGEMENT ROUTES
Route::resource ( 'user', 'UserController', ['except' => []]);
//Route::get('user/{id}/img', 'UserController@image');
Route::post('user/{id}/img', 'UploadController@uploadUserIMG');
Route::post('user/{id}/img/delete', 'UploadController@deleteUserIMG');
Route::post('ship/{shipId}/img', 'UploadController@uploadShipIMG');
Route::resource ( 'user.ship', 'ShipController', ['except' => ['edit', 'create']]);

Route::resource ( 'cashbook', 'CashbookController', ['except' => ['edit', 'delete']] );

Route::post('export/{year}', 'ExportController@exportPDF');

Route::resource ( 'transaction', 'TransactionController', ['except' => ['edit', 'delete']]);
Route::get( 'transaction/open', 'TransactionController@showopen');
Route::get( 'transaction/import', 'TransactionController@showimport');
Route::post( 'transaction/import', 'TransactionController@import');

Route::resource ( 'category', 'CategoryController', ['except' => []]);

Route::resource ( 'account', 'AccountController', ['except' => ['edit']]);
Route::resource ( 'account.balance', 'AccountBalanceController', ['only' => ['store', 'update']]);


// APPLICATION GLOBAL ROUTES
// Main routes
Route::get ( '/', 'DashboardController@show' );

Route::get ( 'home', 'DashboardController@show' );

// function getDashboard() {
// 	if (Gate::denies('manage')) {
// 		return redirect(url('auth/login'));
// 	}
// 	return ();
// }

// Authentication Routes...
Route::get  ( 'auth/login',  'Auth\AuthController@getLogin'  );
Route::post ( 'auth/login',  'Auth\AuthController@postLogin' );
Route::get  ( 'auth/logout', 'Auth\AuthController@getLogout' );
Route::get  ( 'auth/profile', 'Auth\ProfileController@edit' );
Route::post ( 'auth/profile', 'Auth\ProfileController@update' );


