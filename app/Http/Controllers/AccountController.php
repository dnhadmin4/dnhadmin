<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Account;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class AccountController extends Controller
{
    
    public function __construct() {
    	$this->middleware('auth');
    }	
    	 
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if (Gate::denies ( 'manage-users' )) {
			abort ( 403 );
		}
		return view ( 'account/index', [ 
				'items' => Account::orderBy ( 'name', 'asc' )->get (), 
		] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		if (Gate::denies ( 'manage-users' )) {
			abort ( 403 );
		}
		$this->validate ( $request, [ 
				'name' => 'required|max:255',
				'description' => 'max:255',
		] );
		
		$item = Account::create ( [
				'name' => $request ['name'],
				'description' => $request ['description'],
		] );
		
		return Redirect::back()->with('success', $item->name.' is toegevoegd.' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		if (Gate::denies ( 'manage-users' )) {
			abort ( 403 );
		}
		return view ( 'account/show', [ 
				'item' => Account::findOrFail ( $id ), 
		] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		if (Gate::denies ( 'manage-users' )) {
			abort ( 403 );
		}
		$this->validate ( $request, [ 
				'name' => 'required|max:255',
				'description' => 'max:255',
		] );
		
		$item = Account::findorfail ( $id );
		$item->name = $request ['name'];
		$item->description = $request ['description'];
		
		$item->save ();

		return Redirect::back()->with('success', $item->name.' is bijgewerkt.' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (Gate::denies ( 'manage-users' )) {
			abort ( 403 );
		}
		$item = Account::findorfail ( $id );
		
		$item->delete ();
		return Redirect::back()->with('success', $item->name.' is verwijderd.' );
    }
}
