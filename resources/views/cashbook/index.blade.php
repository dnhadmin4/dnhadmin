@extends('layouts.app')

@section('title')
    Kasboek. In kas: <label class="label label-default"><i class="fa fa-eur"></i> {{ $account->currentBalance }}</label>
@endsection

@section('content')
    <div class="btn-toolbar">
        <div class="btn-group">
                 <button class="btn btn-default">Passant</button>
                <button class="btn btn-default">Energiebetaling</button>
                <button class="btn btn-default">Overig</button>
        </div>
            <div class="btn-group" role="group" aria-label="other">
                <button class="btn btn-default">Kascontrole</button>
            </div>
    </div>
    @include('partials.transactiontable', ['transactions' => $account->transactions, 'editCategory' => false])
@endsection

