<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Hash;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [ 
			'username',
			'password' 
	];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [ 
			'password',
			'remember_token' 
	];
	
    /**
     * Get the student's full name.
     *
     * @param  string  $value
     * @return string
     */
    public function getNameFullAttribute($value)
    {
    	$result = $this->name_first . ' ';
    	if (strlen($this->name_middle)>0)
    		$result .= $this->name_middle . ' ';
    	$result .= $this->name_last;
        return $result;
    }
    
	Public function setPasswordAttribute($password)
	{
		return $this->attributes['password'] = Hash::make($password);
	}
	
	public function ships() {
		return $this->hasMany('App\Ship', 'owner_id'); 
	}

	public function getPrimairShipAttribute() {
		return $this->ships()->where('primair', 1)->first();
	}

}
