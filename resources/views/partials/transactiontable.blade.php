	<table class="table table-striped table-hover">
		<thead>
			<th class="col-sm-1 col-lg-1">Datum</th>
			<th class="col-sm-4 col-lg-4">Rekening/Categorie</th>
			<th class="col-sm-1 col-lg-1">Bedrag</th>
			<th class="col-sm-2 col-lg-3">Tegenrekening</th>
			<th class="col-sm-4 col-lg-3">Omschrijving</th>
		</thead>
		<tbody>
			@foreach ($transactions as $t)
			<tr>
				<td class="table-text"><strong>{!! date_create_from_format('Y-m-d', $t->date)->format('d-m \<\b\r\/\> Y') !!}</strong></td>
				<td class="table-text">
					{{ $t->account->description }}<br/>
					@if($editCategory)
					{!! Form::model($t, ['route' => ['transaction.update', $t->id], 'method' => 'put', 'class' => 'form-inline']) !!}
					{!! Form::select('category_id', [null=>'Nog niet gerubriceerd'] + $cats->toArray(), null, ['class' => 'form-control']) !!}
					{!! Form::button('<i class="fa fa-floppy-o"></i>', array('type' => 'submit', 'class' => ($t->category_id==null) ? 'btn btn-warning' : 'btn btn-default')) !!}
					{!! Form::close() !!}
					@else
						@if($t->category)
						<small>{{ $t->category->name }}</small>
						@else
						<small><em>(geen categorie)</em></small>
						@endif
					@endif
				</td>
				<td class="table-text">
					<span class="label label-{{ ($t->debt_credit == 'C') ? 'success' : 'danger' }}"><i class="fa fa-eur"></i> {{ $t->amountString }}</span></td>
				<td class="table-text">{{ $t->counter_name }}<br/>
										<small>{{ $t->counter_account }}&nbsp</small></td>
				<td class="table-text"><abbr title="{{ $t->typetitle }}">{!! $t->typeicon !!}</abbr> {{ $t->description }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
