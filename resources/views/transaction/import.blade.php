@extends('layouts.app') 

@section('tools')
@endsection

@section('title')
	Importeer transacties
@endsection

@section('content')
{!! Form::open(array('url' => url('transaction/import'), 'method' => 'post', 'files' => true)) !!}
<div class="form-group">
	<label class="btn btn-primary" for="import-file-selector">
		<input name="data" id="import-file-selector" type="file" style="display:none;"
			   onchange='$("#import-file-info").html($(this).val().split("\\").pop());'>
		Kies bestand
	</label>
	&nbsp;
	<span id="import-file-info"></span>
</div>
<div class="form-group">
	{!! Form::submit('Submit', array('class' => 'btn btn-info')) !!}
</div>
{!! Form::close() !!}
@endsection

@section('scripts')
@endsection
