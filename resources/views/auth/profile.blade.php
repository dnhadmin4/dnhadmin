@extends('layouts.app')

@section('title')
	Bewerk profiel
@endsection

@section('content')
{!! Form::model($item, ['url' => ['auth/profile'], 'method' => 'post', 'class' => 'form-horizontal']) !!}
<legend>Persoonsgegevens</legend>
<div class="form-group">
	<div class="col-sm-5">
		{!! Form::label('name_first', 'Voornaam*', ['class' => 'control-label']) !!}
		{!! Form::text('name_first', null, ['class' => 'form-control', 'placeholder' => 'De voornaam hier']) !!}
	</div>
	<div class="col-sm-2">
		{!! Form::label('name_middle', 'Tussenvoegsel', ['class' => 'control-label']) !!}
		{!! Form::text('name_middle', null, ['class' => 'form-control', 'placeholder' => '[tussenvoegsel]']) !!}
	</div>
	<div class="col-sm-5">
		{!! Form::label('name_last', 'Achternaam*', ['class' => 'control-label']) !!}
		{!! Form::text('name_last', null, ['class' => 'form-control', 'placeholder' => 'De achternaam hier']) !!}
	</div>
</div>
<div class="form-group">
	<div class="col-sm-6">
		{!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
		{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => '[Het email adres]']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('phone', 'Telefoon', ['class' => 'control-label']) !!}
		{!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => '[telefooonnummer]']) !!}
	</div>
</div>
<legend>Adres</legend>
<div class="form-group">
	<div class="col-sm-10">
		{!! Form::label('addr_street', 'Straat', ['class' => 'control-label']) !!}
		{!! Form::text('addr_street', null, ['class' => 'form-control', 'placeholder' => 'Straatnaam']) !!}
	</div>
	<div class="col-sm-2">
		{!! Form::label('addr_nr', 'Huisnr', ['class' => 'control-label']) !!}
		{!! Form::text('addr_nr', null, ['class' => 'form-control', 'placeholder' => 'bv. 11a']) !!}
	</div>
</div>
<div class="form-group">
	<div class="col-sm-5">
		{!! Form::label('addr_pc', 'Postcode', ['class' => 'control-label']) !!}
		{!! Form::text('addr_pc', null, ['class' => 'form-control', 'placeholder' => '1234 AB']) !!}
	</div>
	<div class="col-sm-7">
		{!! Form::label('addr_city', 'Woonplaats', ['class' => 'control-label']) !!}
		{!! Form::text('addr_city', null, ['class' => 'form-control', 'placeholder' => 'vul plaatsnaam in...']) !!}
	</div>
</div>
<legend>Accountgegevens</legend>
	<div class="form-group">
		<div class="col-sm-6">
			{!! Form::label('username', 'Gebruikersnaam', ['class' => 'control-label']) !!}
			{!! Form::text('username', null, ['class' => 'form-control', 'disabled' => true]) !!}
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-6">
			{!! Form::label('password', 'Wachtwoord', ['class' => 'control-label']) !!}
			{!! Form::password('password', ['class' => 'form-control']) !!}
		</div>
		<div class="col-sm-6">
			{!! Form::label('password_confirmation', 'Bevestig wachtwoord', ['class' => 'control-label']) !!}
			{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-primary">
				<i class="fa fa-btn fa-sign-in"></i> Opslaan
			</button>
		</div>
	</div>
{!! Form::close() !!}
@endsection