@extends('layouts.app') 

@section('title')
	Alle transacties
@endsection

@section('content')
	@include('partials.transactiontable', ['transactions' => $transactions, 'editCategory' => true])
@endsection

