@if ( Auth::check() )
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" 
		   role="button" aria-haspopup="true" aria-expanded="false">
		   	{{ Auth::user()->name_full }} <span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li><a href="{{ url('auth/profile') }}"><i class="fa fa-user"></i>&nbspManage account</a></li>
				<li role="separator" class="divider"></li>
				<li><a href="{{ url('auth/logout') }}"><i class="fa fa-power-off"></i>&nbspLog out</a></li>
			</ul>
	</li>
@else
	<li><a href="{{ url('auth/login') }}">Login</a></li>
@endif
