<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Account;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;

class AccountBalanceController extends Controller
{

    public function __construct() {
        // Enables authorisation
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param mixed $accountid
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($accountid, Request $request)
    {
        if (Gate::denies ( 'manage' )) {
            abort ( 403 );
        }
        $this->validate ( $request, [
            'date' => 'required|date',
            'amount' => 'required|numeric',
        ]);

        $account = Account::findOrFail($accountid);
        $account->balances()->create([
            'date' => $request['date'],
            'amount' => $request['amount'],
       ]);
        return Redirect::back()->with('success','Saldo is correct toegevoegd');
    }

}
