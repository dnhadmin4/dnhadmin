@extends('layouts.app') 

@section('title')
	{{$item->name}} ({{$item->id}})
@endsection

@section('content')
	@include('partials.transactiontable', ['transactions' => $item->transactions, 'editCategory' => false])
@endsection