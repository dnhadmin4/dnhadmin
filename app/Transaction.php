<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'transaction';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
			'account_name',
			'category_id',
			'date',
			'debt_credit', 
			'amount', 
			'counter_account', 
			'counter_name', 
			'type', 
			'description', 
	];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [ 
	];
	
	/**
	 * Get the account that owns the transction.
	 */
	public function account()
	{
		return $this->belongsTo('App\Account', 'account_name', 'name');
	}
	
	/**
	 * Get the category that owns the transction.
	 */
	public function category()
	{
		return $this->belongsTo('App\Category');
	}


	public function getAmountStringAttribute() {
		return number_format($this->amount, 2, ',', '');
	}

	public function getTypeiconAttribute($value) {
		switch($this->type) {
			case 'ba' : return '<i class="fa fa-calculator"></i>';
			case 'id' : return '<i class="fa fa-credit-card"></i>';
			case 'kh' : return '<i class="fa fa-money"></i>';
			default: return "<strong>".$this->type."</strong>";
		}
		
	}
	
	public function getTypetitleAttribute($value) {
		switch($this->type) {
			case 'ac' : return 'acceptgiro';
			case 'ba' : return 'betaalautomaat';
			case 'bg' : return 'bankgiro opdracht';
			case 'cb' : return 'crediteurenbetaling';
			case 'ck' : return 'Chipknip';
			case 'db' : return 'diverse boekingen';
			case 'ei' : return 'euro-incasso';
			case 'ga' : return 'geldautomaat Euro';
			case 'gb' : return 'geldautomaat VV';
			case 'id' : return 'iDEAL';
			case 'kh' : return 'kashandeling';
			case 'ma' : return 'machtiging';
			case 'nb' : return 'NotaBox';
			case 'sb' : return 'salaris betaling';
			case 'sp' : return 'spoedopdracht';
			case 'tb' : return 'eigen rekening';
			case 'tg' : return 'telegiro';
			case 'CR' : return 'tegoed';
			case 'D' : return 'tekort';
			default: return "ONBEKEND";
		}
		
	}	
}
