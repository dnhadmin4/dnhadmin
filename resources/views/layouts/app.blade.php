@extends('layouts.master')

@section('ul-navbar-right')
@if ( Auth::check() )
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" 
		   role="button" aria-haspopup="true" aria-expanded="false">
		   	{{ Auth::user()->name_full }} <span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li><a href="{{ url('auth/profile') }}"><i class="fa fa-user"></i>&nbspManage account</a></li>
				<li role="separator" class="divider"></li>
				<li><a href="{{ url('auth/logout') }}"><i class="fa fa-power-off"></i>&nbspLog out</a></li>
			</ul>
	</li>
@else
	<li><a href="{{ url('auth/login') }}">Login</a></li>
@endif
@endsection

@section('body')
<!-- include the content -->
<div class="row">
	<div class="col-sm-3 col-md-2 sidebar">
		<div class="nav nav-sidebar">
			<ul class="nav nav-pills nav-stacked">
				@include('common.menu')
			</ul>
		</div>
	</div>
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	@if (array_key_exists('title', View::getSections()))
	<div class="page-header"><h1>@yield('title', 'title section here')</h1></div>
	@endif
		{{-- error section --}}
		@include('common.errors')
		@include('common.success')
	
		@yield('content')
				
	</div>
</div>
@endsection