<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    
    public function __construct() {
    	$this->middleware('auth');
    }	
    	 
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if (Gate::denies ( 'manage-users' )) {
			abort ( 403 );
		}
		return view ( 'category/index', [ 
				'items' => Category::orderBy ( 'name', 'asc' )->get (), 
		] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		if (Gate::denies ( 'manage-users' )) {
			abort ( 403 );
		}
		$this->validate ( $request, [ 
				'name' => 'required|max:255',
		] );
		
		$item = Category::create ( [
				'name' => $request ['name'],
		] );
		
		Session::flash ( 'success', $item->name.' is toegevoegd.' );
		return redirect ( 'category' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		if (Gate::denies ( 'manage-users' )) {
			abort ( 403 );
		}
		return view ( 'category/show', [ 
				'item' => Category::findOrFail ( $id ), 
		] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		if (Gate::denies ( 'manage-users' )) {
			abort ( 403 );
		}
		$this->validate ( $request, [ 
				'name' => 'required|max:255',
		] );
		
		$item = Category::findorfail ( $id );
		$item->name = $request ['name'];

		$item->save ();
		
		Session::flash ( 'success', $item->name.' is bijgewerkt.' );
		return redirect ( 'category' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (Gate::denies ( 'manage-users' )) {
			abort ( 403 );
		}
		$item = Category::findorfail ( $id );
		
		$item->delete ();
		Session::flash ( 'success', $item->name . ' is verwijderd.' );
		return redirect ( 'category' );
    }
}
