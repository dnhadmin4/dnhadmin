@extends('layouts.app') 

@section('title')
	({{$item->id}}) {{$item->name_full}} <a class="btn btn-default" href="{{action('UserController@edit', $item->id)}}"><i class="fa fa-pencil"></i></a>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-2">
        <img src="{{ asset('img/user/'.$item->id.'.jpg') }}" alt="pasfoto" class="img-responsive img-thumbnail">
		@if ( $item->id == $user )
		{!! Form::open(array('url'=>'user/'.$item->id.'/img','method'=>'POST', 'files'=>true)) !!}
		<div class="control-group">
			<div class="controls">
				{!! Form::file('image') !!}
				{!! Form::hidden('itemId', $item->id) !!}
				<p class="errors">{!!$errors->first('image')!!}</p>
				@if(Session::has('error'))
					<p class="errors">{!! Session::get('error') !!}</p>
				@endif
			</div>
		</div>
		{!! Form::submit('Upload', array('class'=>'btn btn-primary')) !!}
		{!! Form::close() !!}

		{!! Form::open(array('url'=>'user/'.$item->id.'/img/delete','method'=>'POST')) !!}
		{!! Form::submit('Delete', array('class'=>'btn btn-danger')) !!}
		{!! Form::close() !!}
		@endif
	</div>
	<br/>
	<div class="col-sm-6">
			<i class="fa fa-phone"></i>&nbsp: {{$item->phone}}<br/>
			<i class="fa fa-at"></i>&nbsp: {{$item->email}}<br/>
			<br/>
			<i class="fa fa-home"></i>&nbsp:<br/>
			{{$item->addr_street}} {{$item->addr_nr}}<br/>
			{{$item->addr_pc}}&nbsp&nbsp{{$item->addr_city}}<br/>
	</div>
	<div class="col-sm-3">
			<img src="{{ asset('img/ship/'.$item->primairShip->id.'.jpg') }}" alt="SchipFoto" class="img-responsive img-thumbnail">
	</div>
</div>	
<br/>
<div class="row">
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
    	<li role="presentation" class="active"><a href="#ships" aria-controls="ships" role="tab" data-toggle="tab">Schepen</a></li>
    	<li role="presentation"><a href="#contrib" aria-controls="contrib" role="tab" data-toggle="tab">Contributie</a></li>
	</ul>

  	<!-- Tab panes -->
  	<div class="tab-content">
    	<div role="tabpanel" class="tab-pane active" id="ships">
    		<table class="table table-striped">
    			<thead><tr>
    				<th class="col-sm-1">ID</th>
    				<th class="col-sm-1">
    					<abbr title="Dit schip wordt gebruikt voor het berekenen van de jaarlijkse contributie">Primair</abbr>
    				</th>
    				<th class="col-sm-2">Naam</th>
    				<th class="col-sm-2">Type</th>
    				<th class="col-sm-1">Lengte (m)</th>
    				<th class="col-sm-5"></th>
    			</tr></thead>
    			<tbody>
    			@foreach ($item->ships as $ship)
    			<tr>
    				<td>{{ $ship->id }}</td>
    				<td>{!! ($ship->primair) ? '<i class="fa fa-check-square"></i>' : '' !!}</td>
    				<td>{{ $ship->name }}</td>
    				<td>{{ $ship->type }}</td>
    				<td>{{ $ship->lengthString}}</td>
					<td class="table-text">
						<div class="btn-group">
							<button class="btn btn-primary"
							        onclick="editItem('{{ route('user.ship.update', ['id' => $item->id, 'ship' => $ship->id]) }}', {{ $ship->toJson(JSON_HEX_APOS) }})">
							        <i class="fa fa-pencil-square-o"></i></button>
							<button class="btn btn-danger"
							        onclick="deleteItem('{{ route('user.ship.destroy', ['id' => $item->id, 'ship' => $ship->id]) }}', {{ $ship->toJson(JSON_HEX_APOS) }})">
							        <i class="fa fa-times"></i></button>
							<button class="btn btn-success"
									onclick="uploadIMG('{{ $ship->id }}')">
								<i class="fa fa-camera"></i></button>
						</div>
					</td>
    			</tr>
    			@endforeach
				<tr>
					<td colspan="5"></td>
					<td class="table-text">
						<a class="btn btn-default" onClick="createItem()"><i class="fa fa-plus"></i></a>
					</td>
				</tr>
    			</tbody>
    		</table>
    	</div>
    	<div role="tabpanel" class="tab-pane" id="contrib">...</div>
 	</div>
</div>
@endsection
@section('scripts')
<script>
	/*
	 * Helps setting up the #editItem Modal for creating a new item
	 */
	function createItem() {
		$('#editItem form').attr('action', "{{ route('user.ship.store', [ 'id' => $item->id]) }}");
		$('#editItem form').attr('method', 'post');
		$("#editItem form input[name='_method']").attr('value', 'post');
		$('#editItem').modal();		
	}

	/*
	 * Helps setting up the #editItem Modal for editing an existing item chosen from the list
	 */
	function editItem(href, item) {
		$('#editItem form').attr('action', href);
		$('#editItem form').attr('method', 'post');
		$("#editItem form input[name='_method']").attr('value', 'put');
		$('#editItem form #name').attr('value', item.name);
		$('#editItem form #type').attr('value', item.type);
		$('#editItem form #length').attr('value', item.length);
		$("#editItem form [name='primair']").removeAttr('checked');
		if (item.primair)
			$("#editItem form [name='primair']").attr('checked', true);
		$('#editItem form #id').html(item.id);
		$('#editItem').modal();		
	}

	/*
	 * Helps setting up the #deleteItem Modal for deleting an item chosen from the list
	 */
	function deleteItem(href, item) {
		$('#deleteItem form').attr('action', href);
		$('#deleteItem form #id').html(item.id);
		$('#deleteItem form #name').html(item.name);
		$('#deleteItem').modal();		
	}

	/*
	 * Helps setting up the #uploadIMG Modal for uploading a image of the current ship
	 */
	function uploadIMG(currentShip){
		$('#invisCurrentShip').val(currentShip);
		$('#uploadIMG').modal();
	}

</script>
@endsection

@section('modals')
<!-- Modal -->
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
	  {!! Form::open(['route' => ['user.ship.update', $item->id], 'method' => 'put']) !!}
	    <div class="modal-content">
	      <div class="modal-header">
	        <h4 class="modal-title" id="myModalLabel">Nieuw schip voor <span id="id">{{ $item->name_full }}</span></h4>
	      </div>
	      <div class="modal-body">
			  <div class="form-group">
				  {!! Form::label('name', 'Naam', ['class' => 'control-label']) !!}
				  {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'De naam, vind je meestal voorop het schip']) !!}
			  </div>
			  <div class="form-group">
				  {!! Form::label('type', 'Type', ['class' => 'control-label']) !!}
				  {!! Form::text('type', null, ['id' => 'type', 'class' => 'form-control', 'placeholder' => 'motorboot, zeiljacht, ...']) !!}
			  </div>
			  <div class="form-group">
				  {!! Form::label('length', 'Lengte', ['class' => 'control-label']) !!}
				  {!! Form::text('length', null, ['id' => 'length', 'class' => 'form-control', 'placeholder' => 'Lengte, in meters maar gebruik een punt als komma']) !!}
			  </div>
			  <label>
				  {!! Form::checkbox('primair') !!}
				  Primaire schip van de eigenaar.
			  </label>
		  </div>
	      <div class="modal-footer">
	      	<button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
			<button class="btn btn-primary" type="submit">Opslaan</button>
	       </div>
	    </div>
	  {!! Form::close() !!}
  </div>
</div>
<div class="modal fade" id="deleteItem" tabindex="-1" role="dialog" aria-labelledby="deleteItemLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
	{!! Form::open(['route' => ['user.ship.destroy', $item->id], 'method' => 'delete']) !!}
	    <div class="modal-content">
	      <div class="modal-header">
	        <h4 class="modal-title" id="myModalLabel">Verwijder schip <span id="id"></span></h4>
	      </div>
	      <div class="modal-body">
	      <span id="name"/>
	      </div>
	      <div class="modal-footer">
	      	<button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
			<button class="btn btn-danger" type="submit">Verwijderen</button>
	       </div>
	    </div>
	{!! Form::close() !!}
  </div>
</div>
<div class="modal fade" id="uploadIMG" tabindex="-1" role="dialog" aria-labelledby="uploadIMGLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Upload foto schip <span id="id"></span></h4>
			</div>
			<div class="modal-body">
				{!! Form::open(array('url'=>'ship/'.$ship->id.'/img','method'=>'POST', 'files'=>true)) !!}
				<div class="control-group">
						<div class="controls">
							{!! Form::file('image') !!}
							{!! Form::hidden('itemId', $item->id) !!}
							{!! Form::hidden('currentShip','',array('id' => 'invisCurrentShip')) !!}
							<p class="errors">{!!$errors->first('image')!!}</p>
							@if(Session::has('error'))
								<p class="errors">{!! Session::get('error') !!}</p>
							@endif
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
				{!! Form::submit('Upload', array('class'=>'btn btn-primary')) !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection
