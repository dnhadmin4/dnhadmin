@extends('layouts.app')

@section('body')
<!-- include the content -->
<div class="row">
	<div class="col-sm-3 col-md-2 sidebar">
		<div class="nav nav-sidebar">
			<ul class="nav nav-pills nav-stacked">
				<li><a href="{!! url('/') !!}"><i class="fa fa-tachometer fa-2x fa-fw"></i>&nbspDashboard</a>
				<li class="nav-divider"></li>
				<li>
					<a class="nav-container" data-toggle="collapse" data-parent="#stacked-menu" href="#cb">
						<i class="fa fa-money fa-2x fa-fw"></i>&nbspKasboek<div class="caret-container"><span class="caret arrow"></span></div>
					</a>
					<ul class="nav nav-pills nav-stacked collapse" id="cb">
						<li><a href="{!! url('/') !!}">Overzicht</a></li>						
						<li><a href="{!! url('/') !!}">Invoeren kastransactie</a></li>						
						<li><a href="{!! url('/') !!}">Invoeren storting</a></li>						
					</ul>
				</li>
				<li>
					<a class="nav-container" data-toggle="collapse" data-parent="#stacked-menu" href="#la">
						<i class="fa fa-users fa-2x fa-fw"></i>&nbspLeden<div class="caret-container"><span class="caret arrow"></span></div>
					</a>
					<ul class="nav nav-pills nav-stacked collapse in" id="la">
						<li class="active"><a href="{!! url('user') !!}">Overzicht</a></li>						
						<li><a href="{!! url('/') !!}">Nieuw lid</a></li>						
					</ul>
				</li>
				<li>
					<a class="nav-container" data-toggle="collapse" data-parent="#stacked-menu" href="#ac">
						<i class="fa fa-eur fa-2x fa-fw"></i>&nbspFinanciën<div class="caret-container"><span class="caret arrow"></span></div>
					</a>
					<ul class="nav nav-pills nav-stacked collapse" id="ac">
						<li><a href="{!! url('user') !!}">Rekening Courant</a></li>						
						<li><a href="{!! url('user') !!}">Kas</a></li>						
						<li><a href="{!! url('/') !!}">Spaarrekening</a></li>						
					</ul>
				</li>
				<li>
					<a class="nav-container" data-toggle="collapse" data-parent="#stacked-menu" href="#sg">
						<i class="fa fa-life-ring fa-2x fa-fw"></i>&nbspStamgegevens<div class="caret-container"><span class="caret arrow"></span></div>
					</a>
					<ul class="nav nav-pills nav-stacked collapse" id="sg">
						<li><a href="{!! url('account') !!}">Rekeningen</a></li>						
						<li><a href="{!! url('category') !!}">Categoriën</a></li>						
					</ul>
				</li>
				{{-- show tools section if present --}}
				@if (array_key_exists('tools', View::getSections()))
	                <li class="nav-divider"></li>
					@yield('tools')
				@endif
			</ul>				
		</div>
	</div>
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	@if (array_key_exists('title', View::getSections()))
	<div class="page-header"><h1>@yield('title', 'title section here')</h1></div>
	@endif
		{{-- error section --}}
		@include('common.errors')
		@include('common.success')
	
		@yield('content')
				
	</div>
</div>
@endsection