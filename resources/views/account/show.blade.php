@extends('layouts.app') 

@section('title')
	{{$item->description}} ({{$item->name}})
@endsection

@section('content')
		<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#transactions" aria-controls="transactions" role="tab" data-toggle="tab">Transacties</a></li>
		<li role="presentation"><a href="#balances" aria-controls="balances" role="tab" data-toggle="tab">Saldi</a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="transactions">
			@include('partials.transactiontable', ['transactions' => $item->transactions, 'editCategory' => false])
		</div>
		<div role="tabpanel" class="tab-pane" id="balances">
			@include('partials.balancetable', ['balances' => $item->balances])
			<div><button class="btn btn-default" onclick="createBalance()">Nieuw...</button></div>
		</div>

	</div>

@endsection

@section('scripts')
	<script>
		function createBalance() {
			$('#editBalance form').attr('action', "{{ route('account.balance.store', [ 'id' => $item->id]) }}");
			$('#editBalance form').attr('method', 'post');
			$("#editBalance form input[name='_method']").attr('value', 'post');
			$('#editBalance').modal();
		}
	</script>
@endsection

@section('modals')
	<!-- Modal -->
	<div class="modal fade" id="editBalance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			{!! Form::open(['route' => ['account.balance.update', $item->id], 'method' => 'put']) !!}
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Bewerk saldo <span id="id"></span></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						{!! Form::label('date', 'Datum', ['class' => 'control-label']) !!}
						{!! Form::text('date', null, ['id' => 'date', 'class' => 'form-control', 'placeholder' => 'YYYY-mm-dd']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('amount', 'Bedrag', ['class' => 'control-label']) !!}
						{!! Form::text('amount', null, ['id' => 'amount', 'class' => 'form-control', 'placeholder' => 'in euros']) !!}
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
					<button class="btn btn-primary" type="submit">Opslaan</button>
				</div>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
@endsection
