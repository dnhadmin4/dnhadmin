<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Category;
use App\Account;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $admin = User::create([
        		'username' => 'admin',
        		'password' => 'Trustno1',
        		'name_first' => 'Daan',
        		'name_middle' => 'de',
        		'name_last' => 'Waard',
        		'phone' => '06-23016807',
        		'email' => 'daanwaard@gmail.com',
        ]);
        $admin->ships()->create([
        		'name' => 'Glicca',
        		'type' => 'Zeeschouw',
        		'length' => 8.3,
        		'primair' => true,
        ]);
        Category::create([	'name' => 'Contributie en liggeld leden' ]);
		Category::create([	'name' => 'Passanten' ]);
		Category::create([	'name' => 'Water en Electra, inkomsten' ]);
		Category::create([	'name' => 'Water en Electra, uitgaven' ]);
		Category::create([	'name' => 'Huur perceel & Waterschap' ]);
		Category::create([	'name' => 'Gemeentebelasting (Kliko)' ]);
		Category::create([	'name' => 'Kosten juridisch advies' ]);
		Category::create([	'name' => 'Verzekeringen' ]);
		Category::create([	'name' => 'Bankkosten' ]);
		Category::create([	'name' => 'KvK' ]);
		Category::create([	'name' => 'Onderhoud' ]);
		Category::create([	'name' => 'Ledenbijeenkomsten' ]);
		Category::create([	'name' => 'Transport' ]);
		Category::create([	'name' => 'Administratiekosten' ]);
		
		Account::create([	'name' => 'kas', 
							'description' => 'Kasboek' ]);
		Account::create([	'name' => 'NL56RABO0303801689',
							'description' => 'Rekening courant'	]);
		Account::create([	'name' => 'NL67RABO1021128422',
							'description' => 'Spaarrekening'	]);
		
		Model::reguard();
    }
}
