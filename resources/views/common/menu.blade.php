{{-- DASHBOARD --}}
<li><a href="{!! url('/') !!}"><i class="fa fa-tachometer fa-2x fa-fw"></i>&nbspDashboard</a>

<li class="nav-divider"></li>

{{-- CASHBOOK ADMIN --}}
<li>
	<a class="nav-container" href="{!! url('cashbook') !!}">
		<i class="fa fa-money fa-2x fa-fw"></i>&nbspKasboek
	</a>
</li>

{{-- MEMBER ADMIN --}}
<li>
	<a class="nav-container" href="{!! url('user') !!}">
		<i class="fa fa-users fa-2x fa-fw"></i>&nbspLeden
	</a>
</li>

{{-- FINANCE ADMIN --}}
<li>
	<a class="nav-container" data-toggle="collapse" data-parent="#stacked-menu" href="#ac">
		<i class="fa fa-eur fa-2x fa-fw"></i>&nbspFinanciën<div class="caret-container"><span class="caret arrow"></span></div>
	</a>
	<ul class="nav nav-pills nav-stacked collapse" id="ac">
		<li><a href="{!! url('transaction') !!}">Alle transacties</a></li>						
		<li><a href="{!! url('transaction/open') !!}">Rubriceren</a></li>						
		<li><a href="{!! url('transaction/import') !!}">Importeren</a></li>						
		<li><a href="{!! url('transaction/create') !!}">Invoegen</a></li>						
		<li class="nav-divider"></li>
	</ul>
</li>

{{-- BASIC ADMIN --}}
<li>
	<a class="nav-container" data-toggle="collapse" data-parent="#stacked-menu" href="#sg">
		<i class="fa fa-life-ring fa-2x fa-fw"></i>&nbspStamgegevens<div class="caret-container"><span class="caret arrow"></span></div>
	</a>
	<ul class="nav nav-pills nav-stacked collapse" id="sg">
		<li><a href="{!! url('account') !!}">Rekeningen</a></li>						
		<li><a href="{!! url('category') !!}">Categoriën</a></li>						
	</ul>
	<li class="nav-divider"></li>
</li>

{{-- show tools section if present --}}
@if (array_key_exists('tools', View::getSections()))
	<li class="nav-divider"></li>
	@yield('tools')
@endif
			
