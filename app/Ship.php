<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Ship extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ship';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [ 
			'name',
			'type',
			'owner_id',
			'primair',
			'length' 
	];

	public static function getPrimair($user)
	{
		$id = User::findOrFail ( $user );
		//$results = Ship::get()->where('primair', 1)->and('owner_id', $id->id);

		$results = DB::select("
					SELECT *
                    FROM Ship
                    WHERE primair = 1
                    AND owner_id = ?
                    ", [$id->id]);
		return $results;
	}
}
