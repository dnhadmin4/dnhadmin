<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Transaction;
use App\Category;
use Carbon\Carbon;

class DashboardController extends Controller
{
    	private $monthnames = array(1 => "Januari", 
    				  				2 => "Februari", 
    				  				3 => "Maart", 
    				  				4 => "April", 
    			      				5 => "Mei", 
    				  				6 => "Juni", 
    				  				7 => "Juli", 
    				  				8 => "Augustus", 
    			      				9 => "September", 
    			     				10 => "October", 
    			     				11 => "November", 
    			     				12 => "December");
    
    public function __construct() {
    	$this->middleware('auth');
    }	
    	 
	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('dashboard', [
        		'year' => $this->getYear(),
        		'membercount' => $this->getMembercount(),
        		'uncategorizedTransactionCount' => $this->getUncategorizedTransactionCount(),
        		'yearOverview' => $this->getYearOverview($this->getYear()),
        ]);
    }

    private function getYear() {
    	// TODO make this year dynamic. Choose between current year or a preference
    	return 2015;
    }
    
    private function getMembercount() {
    	return User::count();
    }
    
    private function getYearOverview($year) {
    	$result = array();
    	foreach (Category::get() as $category) {
    		$result[] = $this->getCategoryOverview($year, $category);
    	}
		$result[] = $this->getCategoryOverview($year, null);
    	return $result;
    }

	private function getCategoryOverview($year, $category) {
		$result = new \stdClass();
		$result->name = ($category != null) ? $category->name : '<ongecategoriseerd>';
		$result->transactions = 0;
		$result->debt = 0.0;
		$result->credit = 0.0;

		$start = new \DateTime($year.'-01-01');
		$start->modify('first day of this month');

		$end = new \DateTime($year.'-12-01');
		$end->modify('last day of this month');

		if ($category!=null) {
			$transactions = $category->transactions()
					->whereBetween('date', [$start->format('Y-m-d'), $end->format('Y-m-d')])->get();
		} else {
			$transactions = Transaction::whereNull('category_id')
					->whereBetween('date', [$start->format('Y-m-d'), $end->format('Y-m-d')])->get();
		}
		foreach($transactions as $tr) {
			$result->transactions++;
			if($tr->debt_credit === 'D') {
				$result->debt += $tr->amount;
			} else {
				$result->credit += $tr->amount;
			}
		}
		return $result;
	}
	private function getMonthOverview($year, $month) {
		$result = new \stdClass();
		$result->name = $this->monthnames[$month];
		$result->transactions = 0;
		$result->debt = 0.0;
		$result->credit = 0.0;

		$start = new \DateTime($year.'-'.$month.'-01');
		$start->modify('first day of this month');

		$end = new \DateTime($year.'-'.$month.'-01');
		$end->modify('last day of this month');

		$transactions = Transaction::whereBetween('date', [$start->format('Y-m-d'), $end->format('Y-m-d')])->get();
		foreach($transactions as $tr) {
			$result->transactions++;
			if($tr->debt_credit === 'D') {
				$result->debt += $tr->amount;
			} else {
				$result->credit += $tr->amount;
			}
		}
		return $result;
	}


	private function getUncategorizedTransactionCount() {
    	return Transaction::whereNull('category_id')->count();
    }
    
}
