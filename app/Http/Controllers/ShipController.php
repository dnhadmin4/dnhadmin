<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\User;
use App\Ship;
use Illuminate\Support\Facades\Redirect;

class ShipController extends Controller
{
    
    public function __construct() {
    	$this->middleware('auth');
    }	
    	 
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($ownerid)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($ownerid, Request $request)
    {
		if (Gate::denies ( 'manage' )) {
			abort ( 403 );
		}
		$this->validate ( $request, [ 
				'name' => 'required|max:255',
				'type' => 'required|min:4',
				'length' => 'required|numeric', 
		]);
		
		$owner = User::findOrFail($ownerid);
		$primair = $owner->ships()->count() == 0;
		$owner->ships()->create([
				'name' => $request['name'],
				'type' => $request['type'],
				'primair' => $primair,
				'length' => $request['length'],
		]);
		return redirect(url ('user', [$ownerid]))->with('success','Operation Successful !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $shipId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ownerId, $shipId)
    {
        if (Gate::denies ( 'manage' )) {
            abort ( 403 );
        }
        $this->validate ( $request, [
            'name' => 'required|max:255',
            'type' => 'required|min:4',
            'length' => 'required|numeric',
        ]);

        $ship = Ship::findOrFail($shipId);
        $ship->name = $request['name'];
        $ship->type = $request['type'];
        $ship->length = $request['length'];

        if ($request['primair']) {
            $owner = User::findOrFail($ownerId);
            $owner->ships()->update(['primair' => false]);
            $ship->primair = $request['primair'];
        }

        $ship->save();
        return Redirect::back()->with('success','Operation Successful !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ownerId, $shipId)
    {
        if (Gate::denies ( 'manage' )) {
            abort ( 403 );
        }
        $ship = Ship::findOrFail($shipId);
        $ship->delete();
        return Redirect::back()->with('success','Operation Successful !');
    }
}
