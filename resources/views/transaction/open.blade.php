@extends('layouts.app') 

@section('title')
	Transacties rubriceren
@endsection

@section('content')
	@include('partials.transactiontable', ['transactions' => $transactions, 'editCategory' => true])
@endsection

