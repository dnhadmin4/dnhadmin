<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>DNHAdmin</title>
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
	
	{!! Html::style('css/app.css') !!}
		
</head>

<body>
	<div class="container-fluid">
		<nav class="navbar navbar-inverse  navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<a class="navbar-brand logo" href="{{ url('/') }}"><i class="fa fa-anchor"></i>&nbspDNHAdmin</a>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						@yield('ul-navbar-left')
					</ul>
					<ul class="nav navbar-nav navbar-right">
						@yield('ul-navbar-right')
					</ul>
				</div>
			</div>
		</nav>
		@yield('body')
	</div>
	@yield('modals')
	
	{!! Html::script('js/app.js') !!}
	<!-- include additional scripts -->
	@yield('scripts')
	</body>
</html>