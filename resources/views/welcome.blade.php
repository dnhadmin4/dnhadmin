@extends('layouts.app') 

@section('tools')
@can('manage')
<li role="navigation">
	<a href="{{action('StudentController@index')}}">
		<i class="fa fa-share-square-o"></i>&nbspBeheer studenten
	</a>
</li>
<li role="navigation">
	<a href="{{action('UserController@index')}}">
		<i class="fa fa-share-square-o"></i>&nbspBeheer gebruikers
	</a>
</li>
@endcan
@endsection

@section('title')
	Dashboard
@endsection

@section('content')
<div class="row">
	<div class="col-sm-3">
	</div>
</div>
@endsection

@section('scripts')
@endsection
