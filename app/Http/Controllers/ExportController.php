<?php
namespace App\Http\Controllers;
use App\Category;
use Input;
use Validator;
use Redirect;
use Request;
use Session;
use Auth;
class ExportController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    public function exportPDF($year) {
        $data = Category::all();
        $pdf = \PDF::loadView('pdf.template', compact('data', 'year'));
        return $pdf->stream('jaaroverzicht_'. date("Y") .'.pdf');
    }
}