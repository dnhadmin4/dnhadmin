@extends('layouts.app') 

@section('title')
	Rekeningen <div style="float:right"><button class="btn btn-default" onclick="createItem()">Nieuw...</button></div>
@endsection

@section('content')
	
	@if (count($items) > 0)
		<table class="table table-striped table-hover">
			<thead>
				<th class="col-xs-1 col-sm-1 col-md-1 col-lg-1">ID</th>
				<th class="col-xs-3 col-sm-3 col-md-4 col-lg-4">Naam</th>
				<th class="col-xs-4 col-sm-5 col-md-5 col-lg-5">Omschrijving</th>
				<th class="col-xs-4 col-sm-3 col-md-2 col-lg-2"></i></th>
			</thead>
			<tbody>
				@foreach ($items as $item)
				<tr>
					<td class="table-text">{{ $item->id }}</td>
					<td class="table-text">{{ $item->name }}</td>
					<td class="table-text">{{ $item->description }}</td>
					<td class="table-text">
							<a class="btn btn-default" 
							        href="{{ action('AccountController@show', ['id' => $item->id]) }}">
							        <i class="fa fa-external-link"></i></a>
							<button class="btn btn-primary" 
							        onclick="editItem('{{ action('AccountController@update', ['id' => $item->id]) }}', {{ $item->toJson(JSON_HEX_APOS) }})">
							        <i class="fa fa-pencil-square-o"></i></button>
							<button class="btn btn-danger" 
							        onclick="deleteItem('{{ action('AccountController@update', ['id' => $item->id]) }}', {{ $item->toJson(JSON_HEX_APOS) }})">
							        <i class="fa fa-times"></i></button>
						<div class="btn-group">
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	@endif
@endsection
@section('scripts')
<script>
	function createItem() {
		$('#editItem form').attr('action', '{{ action('AccountController@store') }}');
		$('#editItem form').attr('method', 'post');
		$("#editItem form input[name='_method']").attr('value', 'post');
		$('#editItem').modal();		
	}
	function editItem(href, item) {
		$('#editItem form').attr('action', href);
		$('#editItem form').attr('method', 'post');
		$('#editItem form #name').attr('value', item.name);
		$('#editItem form #description').attr('value', item.description);
		$('#editItem form #id').html(item.id);
		$('#editItem').modal();		
	}
	function deleteItem(href, item) {
		$('#deleteItem form').attr('action', href);
		$('#deleteItem form #id').html(item.id);
		$('#deleteItem form #name').html(item.name);
		$('#deleteItem').modal();		
	}
</script>
@endsection

@section('modals')
<!-- Modal -->
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
	{!! Form::open(['route' => ['category.update', $item->id], 'method' => 'put']) !!}
	    <div class="modal-content">
	      <div class="modal-header">
	        <h4 class="modal-title" id="myModalLabel">Bewerk categorie <span id="id"></span></h4>
	      </div>
	      <div class="modal-body">
				<div class="form-group">
					{!! Form::label('name', 'Naam', ['class' => 'control-label']) !!}
					{!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'korte beschrijving']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('description', 'Omschrijving', ['class' => 'control-label']) !!}
					{!! Form::text('description', null, ['id' => 'description', 'class' => 'form-control', 'placeholder' => 'wat langere beschrijving']) !!}
				</div>
	      </div>
	      <div class="modal-footer">
	      	<button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
			<button class="btn btn-primary" type="submit">Opslaan</button>
	       </div>
	    </div>
	  </div>
	{!! Form::close() !!}
</div>
<div class="modal fade" id="deleteItem" tabindex="-1" role="dialog" aria-labelledby="deleteItemLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
	{!! Form::open(['route' => ['category.destroy', $item->id], 'method' => 'delete']) !!}
	    <div class="modal-content">
	      <div class="modal-header">
	        <h4 class="modal-title" id="myModalLabel">Verwijder categorie <span id="id"></span></h4>
	      </div>
	      <div class="modal-body">
	      <span id="name"/>
	      </div>
	      <div class="modal-footer">
	      	<button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
			<button class="btn btn-danger" type="submit">Verwijderen</button>
	       </div>
	    </div>
	{!! Form::close() !!}
  </div>
</div>
@endsection