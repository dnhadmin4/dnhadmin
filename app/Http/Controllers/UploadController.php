<?php
namespace App\Http\Controllers;
use Input;
use Validator;
use Redirect;
use Request;
use Session;
use Auth;
use File;
use App\Http\Requests;
class UploadController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    //uploads the userIMG and redirects back
    public function uploadUserIMG() {
        $userId = Auth::user()->id; //kan aangepast worden naar itemId zodat de afbeelding van de gebruiker waar je naar kijkt geupdate word in plaats van de ingelogde gebruiker
        $itemId = Input::get('itemId');
        // getting all of the post data
        $file = array('image' => Input::file('image'));
        // setting up rules
        $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return Redirect::to('user/'.$itemId)->withInput()->withErrors($validator);
        }
        else {
            // checking file is valid.
            if (Input::file('image')->isValid()) {
                $destinationPath = 'img/user'; // upload path
                $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = $userId.'.'.$extension; // renameing image
                Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                // sending back with message
                Session::flash('success', 'Upload successfully');
                return Redirect::to('user/'.$itemId);
            }
            else {
                // sending back with error message.
                Session::flash('error', 'uploaded file is not valid');
                return Redirect::to('user/'.$itemId);
            }
        }
    }

    //this function deletes the userIMG and redirects back
    public function deleteUserIMG() {
        $userId = Auth::user()->id;
        $destinationPath = 'img/user/';
        File::Delete($destinationPath.$userId.'.jpg');
        return Redirect::to('user/'.$userId);
    }


    //this function uploads the ship image and redirects back
    public function uploadShipIMG() {
        $itemId = Input::get('itemId');
        $currentShip = Input::get('currentShip');
        // getting all of the post data
        $file = array('image' => Input::file('image'));
        // setting up rules
        $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return Redirect::to('upload')->withInput()->withErrors($validator);
        }
        else {
            // checking file is valid.
            if (Input::file('image')->isValid()) {
                $destinationPath = 'img/ship'; // upload path
                $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = $currentShip.'.'.$extension; // renameing image
                Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                // sending back with message
                Session::flash('success', 'Upload successfully');
                return Redirect::to('user/'.$itemId);
            }
            else {
                // sending back with error message.
                Session::flash('error', 'uploaded file is not valid');
                return Redirect::to('user/'.$itemId);
            }
        }
    }

}