<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'account';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [ 
			'name',
			'description',
	];

	/**
	 * @return mixed the query that gets the matching Transactions of this account.
	 */
	public function transactions() {
		return $this->hasMany('App\Transaction', 'account_name', 'name')->orderBy('date', 'desc');
	}

	/**
	 * @return mixed the query that gets the matching balances of this account.
	 */
	public function balances() {
		return $this->hasMany('App\Balance')->orderBy('date', 'desc');
	}

	public function getCurrentBalanceAttribute() {
		$lastBalance = $this->balances()->first();
		if ($lastBalance == null)
			return 0;
		$debt = $this->transactions()->where('date', '>=', $lastBalance->date)
				->where('debt_credit','=', 'D')->sum('amount');
		$cred = $this->transactions()->where('date', '>=', $lastBalance->date)
				->where('debt_credit','=', 'C')->sum('amount');
		return number_format($lastBalance->amount-$debt+$cred, 2, ',', ' ');
	}

}
