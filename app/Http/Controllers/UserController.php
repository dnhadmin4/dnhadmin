<?php

namespace App\Http\Controllers;

use App\Ship;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller {

	
	public function __construct() {
		$this->middleware('auth');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		if (Gate::denies ( 'manage-users' )) {
			abort ( 403 );
		}
		return view ( 'user/index', [ 
				'users' => User::orderBy ( 'id', 'asc' )->get (), 
		] );
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param int $id        	
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		if (Gate::denies ( 'manage' )) {
			abort ( 403 );
		}
		return view ( 'user/show', [ 
				'item' => User::findOrFail ( $id ),
				'user' => Auth::user()->id,
				'primair' => Ship::getPrimair( $id )
		] );
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		if (Gate::denies ( 'manage' )) {
			abort ( 403 );
		}
		return view ( 'user/create', [] );
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request        	
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		if (Gate::denies ( 'manage-users' )) {
			abort ( 403 );
		}
		$this->validate ( $request, [ 
				'name_first' => 'required|max:255',
				'name_middle' => 'max:255',
				'name_last' => 'required|max:255',
				'email' => 'required|email|max:255',
				'phone' => 'max:255',
				'addr_street' => 'max:255',
				'addr_nr' => 'max:255',
				'addr_pc' => ['max:255', 'regex:/^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i'], //array because of | chars in regex
				'addr_city' => 'max:255',
				'username' => 'required|min:4|max:255|unique:user',
				'password' => 'confirmed|min:6' 
		] );
		
		$user = User::create ( [ 
				'username' => $request ['username'],
		] );
		
		$user->name_first = $request ['name_first'];
		$user->name_middle = $request ['name_middle'];
		$user->name_last = $request ['name_last'];
		$user->email = $request ['email'];
		$user->phone = $request ['phone'];
		$user->addr_street = $request ['addr_street'];
		$user->addr_nr = $request ['addr_nr'];
		$user->addr_pc = $request ['addr_pc'];
		$user->addr_city = $request ['addr_city'];
		
		if (strlen ( $request ['password'] ) > 0) {
			$this->validate ( $request, [ 
					'password' => 'required|confirmed|min:6' 
			] );
			$user->password = bcrypt ( $request ['password'] );
		}

		$user->save ();
		return redirect ( 'user' )->with( 'success', $user->name_full.' is toegevoegd.' );
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id        	
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		if (Gate::denies ( 'manage-users' ) && Gate::denies( 'manage-self', $id)) {
			abort ( 403 );
		}
		return view ( 'user/edit', [ 
				'item' => User::findorfail ( $id ),
		] );
	}
	
	/**
	 * Update the specified resource in storage. 
	 *
	 * @param \Illuminate\Http\Request $request        	
	 * @param int $id        	
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		if (Gate::denies ( 'manage-users' ) && Gate::denies( 'manage-self', $id)) {
			abort ( 403 );
		}
		$this->validate ( $request, [ 
				'name_first' => 'required|max:255',
				'name_middle' => 'max:255',
				'name_last' => 'required|max:255',
				'email' => 'required|email|max:255',
				'phone' => 'max:255',
				'addr_street' => 'max:255',
				'addr_nr' => 'max:255',
				'addr_pc' => ['max:255', 'regex:/^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i'], //array because of | chars in regex
				'addr_city' => 'max:255',
		] );
		
		$user = User::findorfail ( $id );
		$user->name_first = $request ['name_first'];
		$user->name_middle = $request ['name_middle'];
		$user->name_last = $request ['name_last'];
		$user->email = $request ['email'];
		$user->phone = $request ['phone'];
		$user->addr_street = $request ['addr_street'];
		$user->addr_nr = $request ['addr_nr'];
		$user->addr_pc = $request ['addr_pc'];
		$user->addr_city = $request ['addr_city'];
		
		if (strlen ( $request ['password'] ) > 0) {
			$this->validate ( $request, [ 
					'password' => 'required|confirmed|min:6' 
			] );
			$user->password = bcrypt ( $request ['password'] );
		}

		$user->save ();
		
		Session::flash ( 'success', $user->name_full.' is bijgewerkt.' );
		return redirect ( 'user' );
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id        	
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		if (Gate::denies ( 'manage-users' )) {
			abort ( 403 );
		}
		$currentuser = Auth::user ();
		if ($id == $currentuser->id) {
			return Redirect::route ( 'user.edit', [ 
					'id' => $id 
			] )->withErrors ( 'mag ouzelf nie weggooien' );
		}
		
		$user = User::findorfail ( $id );
		
		$user->roles()->detach();
		
		$user->delete ();
		Session::flash ( 'success', 'User is deleted.' );
		return redirect ( 'user' );
	}
}
