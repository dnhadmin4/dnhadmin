@extends('layouts.app') 

@section('title')
	<i class="fa fa-tachometer"></i>&nbsp&nbspDashboard
@endsection

@section('content')
          <div class="row placeholders">
            <div class="col-xs-6 col-sm-6 col-lg-3 placeholder">
            	<div class="jumbotron">
	              	<h1>{{ $year }}</h1>
	              	<h4>Huidige boekjaar</h4>
	              	<span class="text-muted">Voor het jaaroverzicht</span>
		        </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-lg-3 placeholder">
            	<div class="jumbotron">
	              	<h1>{{$membercount}}</h1>
	              	<h4>Actieve Leden</h4>
	              	<span class="text-muted">In de ledenlijst</span>
		        </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-lg-6 placeholder">
            	<div class="jumbotron">
	              	<h1>{{$uncategorizedTransactionCount}}</h1>
	              	<h4>Open transacties</h4>
	              	<span class="text-muted">Nog niet gecategoriseerd</span>
		        </div>
            </div>
		  </div>
          <div  class="sub-header">
            <h2>Huidig financieel jaaroverzicht</h2>
            {!! Form::open(array('url'=>'export/'.$year,'method'=>'POST', 'target'=>'_blank')) !!}
            {!! Form::submit('Download', array('class'=>'send-btn')) !!}
            {!! Form::close() !!}
          </div>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th class="col-lg-3">Categorie</th>
                  <th class="col-lg-2">Transacties</th>
                  <th class="col-lg-2">Debet</th>
                  <th class="col-lg-2">Credit</th>
                  <th class="col-lg-3"></th>
                </tr>
              </thead>
              <tbody>
              @foreach($yearOverview as $category)
                <tr>
                  <td>{{$category->name}}</td>
                  <td>{{$category->transactions}}</td>
                  <td>€ {{$category->debt}}</td>
                  <td>€ {{$category->credit}}</td>
                  <td></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
@endsection

@section('scripts')
@endsection
