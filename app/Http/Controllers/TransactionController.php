<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Transaction;
use Illuminate\Support\Facades\Input;
use App\Category;
use Illuminate\Support\Facades\Redirect;
use App\Account;

class TransactionController extends Controller
{
	
	public function __construct() {
		$this->middleware('auth');
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if (Gate::denies ( 'manage' )) {
			abort ( 403 );
		}
		return view ( 'transaction/index', [ 
				'transactions' => Transaction::orderBy ( 'date', 'desc' )->get (), 
				'cats'         => Category::orderBy ( 'name', 'asc')->lists('name', 'id'),
		] );
    }

	/**
	 * Display a listing of the uncategorized resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function showopen()
    {
		if (Gate::denies ( 'manage' )) {
			abort ( 403 );
		}
		return view ( 'transaction/open', [ 
				'transactions' => Transaction::whereNull('category_id')->orderBy ( 'date', 'asc' )->get (), 
				'cats'         => Category::orderBy ( 'name', 'asc')->lists('name', 'id'),
		] );
    }

    public function showimport() {
    	if (Gate::denies ( 'manage' )) {
    		abort ( 403 );
    	}
    	return view ( 'transaction/import' );
    }
    
    /**
     * Imports a list of transactions in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
		if ($request->hasFile('data')) {
			$file = $request->file('data');
			$handle = fopen($file->getRealPath(), 'r');
			$rowCount=0;
			$imported = 0;
			$skipped = 0;
			while( ($data = fgetcsv($handle)) !== FALSE) {
				$account = $data[0];
				$date = date_create_from_format('Ymd', $data[2]);
				$dc = $data[3];
				$amount = $data[4];
				if (Transaction::where('account_name', $account)
						->whereDate('date', "=", $date->format('Y-m-d'))
						->where('debt_credit', $dc)
						->where('amount', $amount)
						->count() == 0 ) {
					$description = "";
					for ($n=10; $n<16; $n++) {
						if(strlen($data[$n])>0) {
							if (strlen($description)>0)
								$description .= " ";
							$description .= $data[$n];
						}
					}
					Transaction::create([
						'account_name' => $account,
						'date' => $date,
						'debt_credit' => $dc,
						'amount' => $amount,
						'counter_account' => $data[5],
						'counter_name' => $data[6],
						'type' => $data[8],
						'description' => $description,
					]);
					$imported++;
				} else {
					$skipped++;
				}
				$rowCount++;
			}
			$msg = "Succes. ".$imported." transacties geïmporteerd en ".$skipped." overgeslagen.";
			return redirect(url('transaction'))->with('success', $msg);
		}
		return redirect(url('transaction'))->with('error', 'Geen file ontvangen');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if (Gate::denies ( 'manage' )) {
			abort ( 403 );
		}
		return view ( 'transaction/create', [
				'accounts' => Account::orderBy('name')->get(),
				'cats' => Category::orderBy ( 'name', 'asc')->lists('name', 'id'),
		] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		if (Gate::denies ( 'manage' )) {
			abort ( 403 );
		}
		$this->validate ( $request, [
				'account_name' => 'required:exists:account,name',
				'date'         => 'required|date_format:j-m-Y',
				'debt_credit'  => 'required|in:C,D',
				'amount'       => 'required|numeric',
				'category_id'  => 'exists:category,id',
				'description'  => 'required|min:4',
		]);

		// Empty category must be set to null
		if (empty($request['category_id']))
			$category_id = null;
		else
			$category_id = $request ['category_id'];
		$date = \DateTime::createFromFormat('j-m-Y', $request['date']);

		$account = Account::where('name', $request['account_name'])->first();
		$account->transactions()->create([
				'date'        => $date,
				'debt_credit' => $request['debt_credit'],
				'amount'      => $request['amount'],
				'category_id' => $category_id,
				'description' => $request['description'],
		]);

		return Redirect::back()->with('success','Operation Successful !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	if (Gate::denies ( 'manage' )) {
    		abort ( 403 );
    	}
    	$this->validate ( $request, [
    			'category_id' => 'required|exists:category,id',
    	] );
    	
    	$item = Transaction::findorfail ( $id );
    	if (empty($request['category_id']))
    		$item->category_id = null;
    	else
    		$item->category_id = $request ['category_id'];

    	$item->save ();
    	
    	return Redirect::back()->with('success','Operation Successful !');
    	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
