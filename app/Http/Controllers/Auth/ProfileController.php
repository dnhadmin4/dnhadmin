<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view ( 'auth/profile', [
        		'item' => Auth::user(),
        ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
		$this->validate ( $request, [ 
				'name_first' => 'required|max:255',
				'name_middle' => 'max:255',
				'name_last' => 'required|max:255',
				'email' => 'required|email|max:255',
				'phone' => 'max:255',
				'addr_street' => 'max:255',
				'addr_nr' => 'max:255',
				'addr_pc' => ['max:255', 'regex:/^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i'], //array because of | chars in regex
				'addr_city' => 'max:255',
		] );
		
    	$user = Auth::user();
		$user->name_first = $request ['name_first'];
		$user->name_middle = $request ['name_middle'];
		$user->name_last = $request ['name_last'];
		$user->email = $request ['email'];
		$user->phone = $request ['phone'];
		$user->addr_street = $request ['addr_street'];
		$user->addr_nr = $request ['addr_nr'];
		$user->addr_pc = $request ['addr_pc'];
		$user->addr_city = $request ['addr_city'];

    	if (strlen ( $request ['password'] ) > 0) {
			$this->validate ( $request, [ 
					'password' => 'required|confirmed|min:6' 
			] );
			$user->password = bcrypt ( $request ['password'] );
		}

		$user->save();
		return redirect ( '/' )->with('success', 'Je profiel is bijgewerkt.' );
    }

}
