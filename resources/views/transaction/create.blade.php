@extends('layouts.app') 

@section('title')
	Nieuwe transactie toevoegen
@endsection

@section('content')
{!! Form::open(['route' => ['transaction.store'], 'method' => 'post', 'class' => 'form']) !!}
<div class="form-group">
	{!! Form::label('account_name', 'Rekening*', ['class' => 'control-label']) !!}
	@foreach($accounts as $a)
		<div class="radio"><label>{!! Form::radio('account_name', $a->name) !!} {{ $a->description . " (".$a->name.")" }}</label></div>
	@endforeach
</div>
<div class="form-group">
	{!! Form::label('date', 'Datum*', ['class' => 'control-label']) !!}
	<div class='input-group date' id='datetimepicker1'>
		{!! Form::text('date', null, ['class' => 'form-control', 'placeholder' => 'DD-MM-YYYY']) !!}
		<span class="input-group-addon">
			<i class="fa fa-calendar"></i>
		</span>
	</div>
</div>
<div class="form-group">
	{!! Form::label('amount', 'Bedrag*', ['class' => 'control-label']) !!}
	<div class="radio col-sm-2">
		<label>{!! Form::radio('debt_credit', 'D') !!}Af</label>
		&nbsp&nbsp
		<label>{!! Form::radio('debt_credit', 'C') !!}Bij</label>
	</div>
	<div class="input-group col-sm-10">
		<span class="input-group-addon" id="amount_addon"><i class="fa fa-eur"></i></span>
	{!! Form::text('amount', null, ['class' => 'form-control', 'placeholder' => '...', 'aria-describedby' => 'amount_addon']) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('category_id', 'Rubriek*', ['class' => 'control-label']) !!}
	{!! Form::select('category_id', [null=>'Nog niet gerubriceerd'] + $cats->toArray(), null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('description', 'Omschrijving*', ['class' => 'control-label']) !!}
	{!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => '...']) !!}
</div>

<div class="form-group">
	<div>
		<button type="submit" class="btn btn-primary">
			<i class="fa fa-btn fa-sign-in"></i> Opslaan
		</button>
	</div>
</div>
{!! Form::close() !!}
@endsection

@section('scripts')
	<script type="text/javascript">
		$(function () {
			$('#datetimepicker1').datetimepicker({
				'format' : 'DD-MM-YYYY'
			});
		});
	</script>
@endsection