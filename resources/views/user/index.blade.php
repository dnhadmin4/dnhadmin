@extends('layouts.app') 

@section('title')
	Ledenadministratie
	<div style="float:right">
		<a class="btn btn-primary" href="{!! url('user/create') !!}">
			<i class="fa fa-user-plus"></i> Toevoegen...
		</a>
	</div>
@endsection

@section('content')
	@if (count($users) > 0)
		<table class="table table-striped table-hover">
			<thead>
				<th class="col-sm-1">Lidnr</th>
				<th class="col-sm-2">Naam</th>
				<th class="col-sm-2"><i class="fa fa-phone"></i></th>
				<th class="col-sm-2"><i class="fa fa-at"></i></th>
				<th class="col-sm-5"><i class="fa fa-home"></i></th>
			</thead>
			<tbody>
				@foreach ($users as $user)
				<tr class="row-link" style="cursor: pointer;"
					data-href="{{action('UserController@show', ['id' => $user->id]) }}">
					<td class="table-text">{{ $user->id }}</td>
					<td class="table-text">{{ $user->name_full }}</td>
					<td class="table-text">{{ $user->phone }}</td>
					<td class="table-text"><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
					<td class="table-text">
						<a href="http://maps.google.com?q={{ $user->addr_street }}+{{ $user->addr_nr }}+{{ $user->addr_city }}">{{ $user->addr_street }} {{ $user->addr_nr }}, {{ $user->addr_city }}</a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	@endif
@endsection
@section('scripts')
<script>
	jQuery(document).ready(function($) {
	    $(".row-link").click(function() {
	        window.document.location = $(this).data("href");
	    });
	    $('#cohort-tabs a:first').tab('show') // Select first tab
	});	
</script>
@endsection
