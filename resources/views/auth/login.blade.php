@extends('layouts.master')

@section('body')
	<div class="container">
		{{-- error section --}}
		@include('common.errors')
		<div class="col-sm-offset-2 col-sm-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					Login
				</div>

				<div class="panel-body">

					<!-- New Task Form -->
					<form action="{{ url('auth/login')}}" method="POST" class="form-horizontal">
						{{ csrf_field() }}

						<!-- E-Mail Address -->
						<div class="form-group">
							<div class="col-sm-offset-2 input-group col-sm-8">
								<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
								<input type="text" name="username" class="form-control" 
								       placeholder="Username" value="{{ old('username') }}">
							</div>
						</div>

						<!-- Password -->
						<div class="form-group">
							<div class="col-sm-offset-2 input-group col-sm-8">
								<span class="input-group-addon" id="basic-addon1"><i class="fa fa-asterisk"></i></span>
								<input type="password" name="password" class="form-control"
								       placeholder="Password">
							</div>
						</div>

						<!-- Login Button -->
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-6">
								<button type="submit" class="btn btn-default">
									<i class="fa fa-btn fa-sign-in"></i>Login
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection