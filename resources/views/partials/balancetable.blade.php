	<table class="table table-striped table-hover">
		<thead>
			<th class="col-sm-1 col-lg-1">Datum</th>
			<th class="col-sm-1 col-lg-1">Bedrag</th>
		</thead>
		<tbody>
			@foreach ($balances as $item)
			<tr>
				<td class="table-text"><strong>{!! date_create_from_format('Y-m-d', $item->date)->format('d-m-Y') !!}</strong></td>
				<td class="table-text">
					<span class="label label-{{ ($item->debt_credit == 'C') ? 'success' : 'danger' }}"><i class="fa fa-eur"></i> {{ $item->amount }}</span>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
