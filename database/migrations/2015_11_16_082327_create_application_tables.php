<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateApplicationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ship', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type');
            $table->integer('owner_id');
            $table->decimal('length', 6, 2);
            $table->boolean('primary'); // The primary ship of the owner, uses the yearly contributiosn fares 
            $table->timestamps();
        });
        Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64);
            $table->timestamps();
        });
        Schema::create('account', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64)->unique;
            $table->string('description');
            $table->timestamps();
        });
        Schema::create('transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account_name', 128);
            $table->integer('category_id')->nullable()->default(null);
            $table->date('date');
            $table->string('debt_credit', 1);
            $table->decimal('amount', 8, 2);
            $table->string('counter_account', 128);
            $table->string('counter_name');
            $table->string('type', 2);
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaction');
        Schema::drop('account');
        Schema::drop('category');
        Schema::drop('ship');
    }
}
